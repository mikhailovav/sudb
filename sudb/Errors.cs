﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sudb
{
    internal static class Errors
    {
        public static Dictionary<ErrorCode, string> Messages = new Dictionary<ErrorCode, string>()
        {
            {ErrorCode.Unknown,"Непредвиденная ошибка:" },

            {ErrorCode.InvalidName,"Ошибка в имени." },

            { ErrorCode.TypeResolving,"Ошибка получения внутреннего типа СУБД на основе предоставленного." },
            {ErrorCode.TypeVarLenType, "Ошибка сопоставления типа поля СУБД и его длины." },

            {ErrorCode.FileNotFound, "Ошибка доступа к файлу. Файл не найден." },
            {ErrorCode.FileLocked, "Ошибка доступа к файлу. Файл заблокирован." },
            {ErrorCode.FileBrocken, "Ошибка доступа к файлу. Структура файла содержит ошибки." },
            {ErrorCode.FileAlreadyExists, "Ошибка доступа к файлу. Файл уже существует." },
            {ErrorCode.DirectoryNotFound, "Ошибка доступа к папке. Папка не существует." },
            {ErrorCode.DirectoryAlreadyExists, "Ошибка доступа к папке. Папка уже существует." },

            {ErrorCode.FormatterInvalidVersion,"Неподдерживаемая версия формата файла." },
            {ErrorCode.StoreFormatterInvalidRowKey,"Неожиданное несовпадение системных ключей в файле хранилища." },

            {ErrorCode.ProtoAlreadyExists,"Прототип уже существует." },
            {ErrorCode.ProtoObjectInvalid,"Прототип не распознан." },

            {ErrorCode.ClusteredIndexKeyNotFound,"Ключ не обнаружен в кластерном индексе." },

            {ErrorCode.SudbZeroStore,"Отсутствуют доступные хранилища." },
        };

        public static string GetMessage(ErrorCode code)
        {
            return Messages[code];
        }
    }

    public enum ErrorCode : int
    {
        Unknown = 0,

        InvalidName=10,

        TypeResolving = 20,
        TypeVarLenType=21,

        FileNotFound=40,
        FileLocked=41,
        FileBrocken=42,
        FileAlreadyExists=43,
        DirectoryNotFound =44,
        DirectoryAlreadyExists = 45,


        FormatterInvalidVersion = 71,


        StoreFormatterInvalidRowKey=130,

        ProtoAlreadyExists=340,
        ProtoObjectInvalid=341,

        ClusteredIndexKeyNotFound = 420,

        SudbZeroStore=600,
    }

    [Serializable]
    public class SudbException : Exception
    {

        public SudbException()
        {
        }

        public readonly ErrorCode ErrorCode;

        public SudbException(ErrorCode code) : base(Errors.GetMessage(code))
        {
            ErrorCode = code;
        }

        public SudbException(ErrorCode code, Exception inner) : base(Errors.GetMessage(code), inner)
        {
            ErrorCode = code;
        }

        protected SudbException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
