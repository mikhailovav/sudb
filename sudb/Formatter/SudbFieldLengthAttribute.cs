using System;

namespace Sudb.Formatter
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class SudbFieldLengthAttribute : Attribute
    {
        public uint Length { get; set; }

        public SudbFieldLengthAttribute(uint length)
        {
            this.Length = length;
        }
    }
}