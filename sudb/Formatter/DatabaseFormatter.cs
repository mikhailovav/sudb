using System;
using System.IO;
using System.Text;
using Sudb.MetaData;

namespace Sudb.Formatter
{
    internal class DatabaseFormatter
    {
        /*context: 
        [start 2byte]
        [formatter version 1byte]
        [db version 2byte]
        [name][0]
        [proto-array-lenght 2byte]
        [struct-array-length 2byte]
        [indexe-array-length 2byte]
        [proto array! *byte]
        [struct array! *byte]
        [index array! *byte]
        [end 2byte]
        [crc checksum]
        */

        /*proto:
        [name][0]
        [fields-lenght 2byte]
        [fields array! *byte]
        */
        private static byte[] Start = new byte[] {0xD0, 0xB0};//START:[0xD0],[0xB0]
        private static byte[] End = new byte[] {0xE0, 0x00,0xFF};//....[0xE0][0x00][0xFF]:EOF        

        public const byte FormatterVersion = 0x01;

        public DatabaseMetaData Deserialize(Stream serializationStream)
        {
            throw new NotImplementedException();
        }

        public void Serialize(Stream serializationStream, DatabaseMetaData item)
        {
            using (var memory = new MemoryStream())
            {
                using (var writer = new BinaryWriter(memory, Encoding.UTF8, true))
                {


                    writer.Seek(0, 0);
                    writer.Write(Start);
                    writer.Write(FormatterVersion);
                    writer.Write(item.Version);
                    writer.Write(item.Name);
                    //StructureProto
                    writer.Write(Convert.ToUInt16(item.Prototypes.Length));
                    foreach (var proto in item.Prototypes)
                    {
                        writer.Write(proto.Name);
                        writer.Write(proto.Key);
                        writer.Write(Convert.ToUInt16(proto.Fields.Length));
                        foreach (var field in proto.Fields)
                        {
                            writer.Write(field.Name);
                            writer.Write((byte)field.BaseType);
                        }
                    }
                    //StructureProto end

                    //StructureMeta
                    writer.Write(Convert.ToUInt16(item.Structs.Length));
                    foreach (var str in item.Structs)
                    {
                        writer.Write(str.Name);
                        writer.Write(str.Protokey);
                        writer.Write(str.ProtoSubtype);
                        foreach (var field in str.FieldsOverride)
                        {
                            writer.Write(field.Name);
                            writer.Write((byte)field.BaseType);
                        }
                    }
                    //StructureMeta end

                    //Index
                    writer.Write(Convert.ToUInt16(item.Indexes.Length));
                    foreach (var index in item.Indexes)
                    {
                        writer.Write(index.Name);
                        writer.Write(index.Protokey);
                        writer.Write((byte)index.Type);
                    }
                    //Index end
                    writer.Write(End);
                    writer.Write(Crc16.ComputeChecksum(memory.GetBuffer()));
                }
                serializationStream.Seek(0, 0);
                memory.Seek(0, 0);
                memory.CopyTo(serializationStream);
            }
        }
    }
}