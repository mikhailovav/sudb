using System;

namespace Sudb.Formatter
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class SudbIgnoreAttribute : Attribute
    {

    }
}