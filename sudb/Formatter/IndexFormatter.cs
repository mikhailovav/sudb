using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Sudb.MetaData;

namespace Sudb.Formatter
{
    internal class IndexFormatter
    {
        private static byte[] Start = new byte[] { 0xE0, 0xB6 };//START:[0xD0],[0xB0]
        public const byte FormatterVersion = 0x01;

        public Dictionary<uint, LocationSelector> Load(Stream stream)
        {
            var index = new Dictionary<uint, LocationSelector>();
            stream.Seek(0, 0);
            using (var reader = new BinaryReader(stream, Encoding.UTF8))
            {
                if (reader.ReadByte() != Start[0] || reader.ReadByte() != Start[1])
                    throw new SudbException(ErrorCode.FileBrocken);
                if (reader.ReadByte() != FormatterVersion)
                    throw new SudbException(ErrorCode.FormatterInvalidVersion);
                var length = reader.ReadUInt32();
                while (length != 0)
                {
                    length--;
                    var key = reader.ReadUInt32();
                    var store = reader.ReadByte();
                    var pos = reader.ReadUInt64();
                    index.Add(key, new LocationSelector(store, pos));
                }
            }
            return index;
        }

        public void Write(Stream stream, Dictionary<uint, LocationSelector> index)
        {
            using (var writer = new BinaryWriter(stream, Encoding.UTF8, true))
            {
                writer.Seek(0, 0);
                writer.Write(Start);
                writer.Write(FormatterVersion);
                writer.Write(index.Count);
                foreach (var ind in index)
                {
                    writer.Write(ind.Key);
                    writer.Write(ind.Value.StoreNumber);
                    writer.Write(ind.Value.Position);
                }
            }
        }
    }
}