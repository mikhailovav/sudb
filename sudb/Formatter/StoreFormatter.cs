using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using Sudb.MetaData;

namespace Sudb.Formatter
{
    internal class StoreFormatter
    {
        /*
        context:
        [optStart 2byte]
        [formatterVersion]
        [protoKey 2byte]
        [protoSubtype 4byte]
        [operationalNumber 1byte]
        [array length 4byte]
        [array*]
        */
        private static byte[] Start = new byte[] { 0xF0, 0xB1 };//START:[0xF0],[0xB1]
        private static byte[] End = new byte[] { 0xE0, 0x00, 0xFF };//....[0xE0][0x00][0xFF]:EOF  

        public const byte FormatterVersion = 0x01;

        public void Deserialize(Stream serializationStream, out ObjectData[] result, out byte number)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// �������������� ��������������� ObjectData[], ����� ������ ���� ������� 
        /// ��������� ����������� ��������� �� ��������� � �������� ���� ���������
        /// </summary>
        /// <param name="serializationStream"></param>
        /// <param name="storage"></param>
        /// <param name="storageNumber"></param>
        /// <param name="objects"></param>
        /// <param name="index"></param>
        public void WriteChanges(Stream serializationStream, StoreMetaData storage, IndexMetaData index)
        {
            using (var writer = new BinaryWriter(serializationStream))
            {
                using (var reader = new BinaryReader(serializationStream))
                {
                    foreach (var obj in storage.ModifiedObjects.Where(w => w.State == ModificationKey.Modified || w.State == ModificationKey.Deleted))
                    {
                        var pos = index.GetPosition(obj.Key);
                        serializationStream.Seek(Convert.ToInt64(pos), SeekOrigin.Begin);
                        var key = reader.ReadUInt32();
                        if(key!=obj.Key)
                            throw new SudbException(ErrorCode.StoreFormatterInvalidRowKey);
                        if (obj.State == ModificationKey.Modified)
                        {
                            writer.Write((byte)0);//not deleted
                            var item = obj.Data as IDictionary<string, object>;
                            foreach (var field in storage.Fields)
                            {
                                WriteBytes(writer, field.BaseType, item[field.FieldName], field.VarLength);
                            }
                        }
                        else
                        {
                            writer.Write((byte)1);//deleted
                        }
                    }
                    serializationStream.Seek(0, SeekOrigin.End);
                    foreach (var obj in storage.ModifiedObjects.Where(w => w.State == ModificationKey.Added))
                    {
                        var pos = Convert.ToUInt64(serializationStream.Position);
                        index.Set(obj.Key, pos, storage.Number);
                        writer.Write(obj.Key);
                        writer.Write((byte)0);//not deleted
                        var item = obj.Data as IDictionary<string, object>;
                        foreach (var field in storage.Fields)
                        {
                            WriteBytes(writer, field.BaseType, item[field.FieldName], field.VarLength);
                        }
                    }
                    
                }
                
            }

        }

        public static void WriteBytes(BinaryWriter wr,ItemType t, object obj,uint length)
        {
            switch (t)
            {
                case ItemType.Null:
                    return;                   
                case ItemType.StShort:
                    wr.Write((short)obj);
                    return;
                case ItemType.StInt32:
                    wr.Write((int)obj);
                    return;
                case ItemType.StInt64:
                    wr.Write((long)obj);
                    return;
                case ItemType.StBool:
                    wr.Write((bool)obj);
                    return;
                case ItemType.StByte:
                    wr.Write((byte)obj);
                    return;
                case ItemType.StNVarChar:
                    throw new NotImplementedException("sdf");
                case ItemType.StVarByte:
                    throw new NotImplementedException("sdf");
                case ItemType.StFloat:
                    wr.Write((float)obj);
                    return;
                case ItemType.StDouble:
                    wr.Write((double)obj);
                    return;
                case ItemType.StUuid:
                    throw new NotImplementedException("sdf");
                case ItemType.Reference:
                    throw new NotImplementedException("sdf");
                case ItemType.Foreign:
                    throw new NotImplementedException("sdf");
                default:
                    throw new SudbException(ErrorCode.TypeResolving);
            }
        }
        public void Create(Stream serializationStream, byte number, ObjectData[] objects)
        {
            using (var memory = new MemoryStream())
            {
                using (var writer = new BinaryWriter(memory, Encoding.UTF8, true))
                {
                    writer.Seek(0, 0);
                    writer.Write(Start);
                    writer.Write(FormatterVersion);

                }
                serializationStream.Seek(0, 0);
                memory.Seek(0, 0);
                memory.CopyTo(serializationStream);
            }
        }
    }
}