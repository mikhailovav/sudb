using System;

namespace Sudb.Formatter
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class SudbFieldAttribute : Attribute
    {
        public string Name { get; set; }

        public SudbFieldAttribute(string name)
        {
            this.Name = name;
        }
    }
}