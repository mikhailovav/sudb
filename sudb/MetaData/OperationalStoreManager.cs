using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Sudb.MetaData
{
    internal class OperationalStoreManager:IReadOnlyDictionary<StoreSelector, StoreMetaData>
    {
       
        public OperationalStoreManager(StoreMetaData[] items)
        {
            _store = new Dictionary<StoreSelector, StoreMetaData>();
            _numbers= new Dictionary<StructSelector, byte>();
            foreach (var item in items)
            {
                _store.Add(new StoreSelector(item.Protokey,item.ProtoSubtype,item.Number), item);
                var s = new StructSelector(item.Protokey, item.ProtoSubtype);
                if (_numbers.ContainsKey(s))
                {
                    if (_numbers[s] < item.Number)
                        _numbers[s] = item.Number;
                }
                else
                {
                    _numbers[s] = 0;
                }
            }
        }

        public OperationalStoreManager()
        {
            _store = new Dictionary<StoreSelector, StoreMetaData>();
            _numbers= new Dictionary<StructSelector, byte>();
        }

        private readonly Dictionary<StructSelector, byte> _numbers;

        private readonly Dictionary<StoreSelector, StoreMetaData> _store;

        public IEnumerator<KeyValuePair<StoreSelector, StoreMetaData>> GetEnumerator()
        {
            return _store.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int Count => _store.Count;

        public bool ContainsKey(StoreSelector key)
        {
            return _store.ContainsKey(key);
        }

        public bool TryGetValue(StoreSelector key, out StoreMetaData value)
        {
            var result =  _store.TryGetValue(key, out value);
            return result;
        }

        public StoreMetaData this[StoreSelector key] => _store[key];

        public IEnumerable<StoreSelector> Keys => _store.Keys;
        public IEnumerable<StoreMetaData> Values => _store.Values;


        public void Insert(ObjectData data)
        {
            if (_store.Count == 0)
            {
                //_store[new StoreSelector(data.Protokey, data.ProtoSubtype, 0)] = new StoreMetaData(data.Protokey, data.ProtoSubtype, new List<ObjectData>(new[] { data }), 0);
                //return;
                throw new SudbException(ErrorCode.SudbZeroStore);
            }
            var op = _store.LastOrDefault();
            var n = _numbers[new StructSelector(data.Protokey, data.ProtoSubtype)];
            n++;
            _store[new StoreSelector(data.Protokey, data.ProtoSubtype, n)].ModifiedObjects.Add(data);
        }

        public void Modify(StoreSelector selector, int key, ObjectData data)
        {
            var item = _store[selector].ModifiedObjects.Single(s => s.Key == data.Key);
            _store[selector].ModifiedObjects.Remove(item);
            _store[selector].ModifiedObjects.Add(data);
        }

        public void Delete(StoreSelector selector, int key, ObjectData data)
        {
            _store[selector].ModifiedObjects.Single(s => s.Key == data.Key).State = ModificationKey.Deleted;
        }
    }
}