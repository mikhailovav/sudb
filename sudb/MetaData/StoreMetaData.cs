using System.Collections.Generic;

namespace Sudb.MetaData
{
    internal class StoreMetaData
    {
        public readonly string Filepath;

        public readonly ushort Protokey;
        public readonly uint ProtoSubtype;
        public readonly List<ObjectData> ModifiedObjects;

        public readonly byte Number;
        public readonly ProtoFieldForType[] Fields;

        public StoreMetaData(ushort protokey, uint protoSubtype, List<ObjectData> modifiedObjects, byte number,ProtoFieldForType[] fields, string filepath)
        {
            Protokey = protokey;
            ProtoSubtype = protoSubtype;
            ModifiedObjects = modifiedObjects;
            Number = number;
            Fields = fields;
            Filepath = filepath;
        }
    }
}