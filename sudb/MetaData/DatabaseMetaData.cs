using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using Sudb.Formatter;

namespace Sudb.MetaData
{
    /// <summary>
    /// ���������� � ���� ������
    /// </summary>
    public class DatabaseMetaData
    {
        private readonly Dictionary<ushort,ProtoMetaData> _prototypes;
        private readonly List<StructMetaData> _structures;
        private readonly List<IndexMetaData> _indexes;

        private readonly OperationalStoreManager _operationalSoreManager;


        public readonly string Name;

        public readonly string Filename;
        public readonly string WorkDirectory;

        public string DataDirectory => Path.Combine(WorkDirectory+"/", Filename + "_data");

        public string DbFilepath => Path.Combine(WorkDirectory + "/", Filename);

        /// <summary>
        /// ������ ��, ������������� ��� ��������� ��������
        /// </summary>
        public ushort Version { get;  }

        public ProtoMetaData[] Prototypes => _prototypes.Values.ToArray();

        public StructMetaData[] Structs => _structures.ToArray();

        internal IndexMetaData[] Indexes => _indexes.ToArray();

        public bool Ready { get; private set; }

        private DatabaseMetaData(string name, string filename, string workDirectory)
        {
            this.Name = name;
            Filename = filename;
            WorkDirectory = workDirectory;
            Version = 1;
            _prototypes = new Dictionary<ushort, ProtoMetaData>();
            _structures = new List<StructMetaData>();
            _indexes = new List<IndexMetaData>();
            _operationalSoreManager = new OperationalStoreManager();
        }
        private DatabaseMetaData(string name, string filename, string workDirectory, ushort version, ProtoMetaData[] protoMetaData, StructMetaData[] structure, IndexMetaData[] index)
        {
            this.Name = name;
            Version = version;
            WorkDirectory = workDirectory;
            Filename = filename;
            _prototypes = new Dictionary<ushort, ProtoMetaData>();
            foreach (var p in protoMetaData)
            {
                _prototypes.Add(p.Key,p);
            }
            _structures = new List<StructMetaData>(structure);
            _indexes = new List<IndexMetaData>(index);
            _operationalSoreManager = new OperationalStoreManager();
        }

        internal static DatabaseMetaData Load(string name, string filename, string workDirectory, ushort version, ProtoMetaData[] protoMetaData, StructMetaData[] structure, IndexMetaData[] index)
        {
            var db = new DatabaseMetaData(name, filename, workDirectory, version, protoMetaData,structure,index);

            var op = new StoreFormatter();
            
            throw new NotImplementedException();

            db.Ready = true;
            return db;
        }

        private static bool _checkName(string name)
        {
            return Path.GetInvalidFileNameChars().All(a => !name.Contains(a)) &&
                   Path.GetInvalidPathChars().All(a => !name.Contains(a)) && !name.EndsWith(" ") && !name.StartsWith(" ") && !name.EndsWith(".");
        }

        public static DatabaseMetaData Create(string name,string filename, string workDirectory)
        {
            if (!_checkName(name))
                throw new SudbException(ErrorCode.InvalidName);

            if (!Directory.Exists(workDirectory))
                throw new SudbException(ErrorCode.DirectoryNotFound);

            var dir = Path.Combine(workDirectory + "/", filename + "_data");
            
            if (Directory.Exists(dir))
                throw new SudbException(ErrorCode.DirectoryAlreadyExists);

            if (File.Exists(Path.Combine(workDirectory + "/", filename)))
                throw new SudbException(ErrorCode.FileAlreadyExists);

            Directory.CreateDirectory(dir);
#if (DEBUG)
            System.Diagnostics.Debug.Write($"Create directory: {dir}");
#endif
            var db = new DatabaseMetaData(name, filename, workDirectory);
            var formatter =  new DatabaseFormatter();
            formatter.Serialize(db._getDbStream(),db);
            return db;
        }

        public void AddProto(string name, ProtoField[] fields)
        {
            if (!_checkName(name))
                throw new SudbException(ErrorCode.InvalidName);
            if (_prototypes.Values.Any(p => p.Name == name))
                throw new SudbException(ErrorCode.ProtoAlreadyExists);
            

            var key = _prototypes.Count == 0 ? (ushort)1 : _prototypes.Max(m => m.Key);
            key++;
            var proto = new ProtoMetaData(name, key,fields);            
            _prototypes.Add(key,proto);
            _createSystemIndex(proto.Name);
        }

        private void _createSystemIndex(string protoName)
        {
            var filepath = Path.Combine(DataDirectory + "/", protoName + ".idat");
            if(File.Exists(filepath))
                throw new SudbException(ErrorCode.FileAlreadyExists);
            using (var stream = File.Create(filepath))
            {
                var formatter = new IndexFormatter();
                formatter.Write(stream,new Dictionary<uint, LocationSelector>());
            }
        }

        public void AddStructure(string protoName, ProtoField[] overrides)
        {
            if (_prototypes.All(p => p.Value.Name != protoName))
                throw new SudbException(ErrorCode.InvalidName);
           
            var proto = _prototypes.FirstOrDefault(p => p.Value.Name == protoName);
            var protos = _structures.Where(w => w.ProtoName == protoName).ToArray();

            uint subtype = !protos.Any()?(uint)0: protos.Max(m => m.ProtoSubtype);
            subtype++;

            var structname = protoName + subtype;
            var filepath = Path.Combine(DataDirectory + "/", structname + ".dat");

            if (File.Exists(filepath))
                throw new SudbException(ErrorCode.FileAlreadyExists);

            var str = new StructMetaData(filepath, structname, protoName, proto.Key, subtype, overrides);

            using (var stream = File.Create(filepath))
            {
                var formatter = new StoreFormatter();
                formatter.Create(stream,0,new ObjectData[0]);
            }
            _structures.Add(str);
            proto.Value.Structures.Add(str.ProtoSubtype,str);
        }

        public void InsertObject(ushort protoKey, uint protoSubtype, ExpandoObject obj)
        {
            var proto = _prototypes[protoKey];
            var str = proto.Structures[protoSubtype];
            var fields = str.FieldsOverride.ToList();
            fields.AddRange(proto.Fields.Where(a => str.FieldsOverride.All(o => a.Name != o.Name)));

            var data = new ObjectData(protoKey, protoSubtype, fields.ToArray(), obj, ModificationKey.Added, str.GetNewKey());
            _operationalSoreManager.Insert(data);
        }

        public void InsertObject<T>(string protoName, T obj) where T:new()
        {
            InsertObject(protoName, typeof (T), obj);
        }

        public void InsertObject(string protoName, Type t, object obj)
        {
            if (_prototypes.All(p => p.Value.Name != protoName))
                throw new SudbException(ErrorCode.InvalidName);

            var proto = _prototypes.FirstOrDefault(p => p.Value.Name == protoName).Value;

            var objectType = _readObjectType(t);
            objectType.Protokey = proto.Key;
            if (objectType.ProtoSubtype <= 0)
            {
                if(!objectType.CheckProto(proto.Fields))
                    throw new SudbException(ErrorCode.ProtoObjectInvalid);
                var found = false;
                foreach (var meta in proto.Structures.Values)
                {
                    if (objectType.CheckStructure(meta.FieldsOverride))
                    {
                        objectType.ProtoSubtype = meta.ProtoSubtype;
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    //�������� ������ meta
                    throw new NotImplementedException();
                }
            }
            var str = proto.Structures[objectType.ProtoSubtype];
            var data = new ObjectData(objectType.Protokey, objectType.ProtoSubtype, objectType.Fields, t, obj, ModificationKey.Added, str.GetNewKey());
            _operationalSoreManager.Insert(data);
        }

        /// <summary>
        /// ����� �� �����������
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filter"></param>
        /// <returns></returns>
        public T[] GetObjects<T>(string proto, Dictionary<string,object> filter) where T:new()
        {
            throw new NotImplementedException();
        }

        private Stream _dbStream;
        private Stream _getDbStream()
        {
            return _dbStream ?? (_dbStream = File.OpenWrite(Path.Combine(DbFilepath)));
        }


        private readonly Dictionary<string,Stream> _fileStreams = new Dictionary<string, Stream>();
        private Stream _getFileStream(string filename)
        {
            if (!_fileStreams.ContainsKey(filename))
                _fileStreams[filename] = File.OpenWrite(filename);
            return _fileStreams[filename];
        }        

        /// <summary>
        /// ������ ����������� ���������
        /// </summary>
        /// <returns></returns>
        public void FixChanges()
        {
            var dbFrm = new DatabaseFormatter();
            var indxFrm = new IndexFormatter();
            var strFrm = new StoreFormatter();

            foreach (var store in _operationalSoreManager.Values)
            {
                var index = Indexes.Single(i => i.Type == IndexType.SystemClusteredIndex && i.Protokey == store.Protokey);
                strFrm.WriteChanges(_getDbStream(),store, index);
                var changed = false;
                foreach (var item in store.ModifiedObjects.Where(s => s.State == ModificationKey.Added || s.State == ModificationKey.Modified))
                {
                    changed = true;
                    item.State = ModificationKey.None;
                }
                foreach (var item in store.ModifiedObjects.Where(s => s.State == ModificationKey.Deleted).ToArray())
                {
                    changed = true;
                    store.ModifiedObjects.Remove(item);
                }
                if (changed)
                {
                    indxFrm.Write(_getFileStream(index.Filepath),index.Index);
                }
            }
            dbFrm.Serialize(_getDbStream(),this);
        }

        /// <summary>
        /// ����������� ����� ����, ������������� �������� ���������� �� �������� �������, �������������� ������
        /// </summary>
        /// <returns></returns>
        public async Task OptimizeObjects()
        {
            throw new NotImplementedException();
        }


        private static Dictionary<string, StructureProtoPre> ObjectTypeMap = new Dictionary<string, StructureProtoPre>();

        private StructureProtoPre _readObjectType(Type t)
        {
            if (!ObjectTypeMap.ContainsKey(t.FullName))
            {
                var result = new List<ProtoFieldForType>();
                var properties =
                    t.GetProperties()
                        .Where(p => p.CanRead && p.CanWrite);
                foreach (var property in properties)
                {
                    if(property.GetCustomAttribute<SudbIgnoreAttribute>() != null)
                        continue;
                    
                    var name = property.Name;
                    var field = property.GetCustomAttribute<SudbFieldAttribute>();
                    if (field != null)
                        name = field.Name;
                    var length = property.GetCustomAttribute<SudbFieldLengthAttribute>();
                    
                    var len = length?.Length ?? 0;
                    result.Add(new ProtoFieldForType(name, property.Name, _getType(property.PropertyType), property.PropertyType,len));
                }
                ObjectTypeMap[t.FullName] = new StructureProtoPre(t.FullName, result.ToArray());
            }            
            return ObjectTypeMap[t.FullName];
        }

        private ItemType _getType<T>()
        {
            return _getType(typeof (T));
        }
        private ItemType _getType(Type t)
        {
            if (t == typeof(DBNull))
                return ItemType.Null;
            if (t == typeof(short))
                return ItemType.StShort;
            if (t == typeof(Int32))
                return ItemType.StInt32;
            if (t == typeof(Int64))
                return ItemType.StInt64;
            if (t == typeof(bool))
                return ItemType.StBool;
            if (t == typeof(byte))
                return ItemType.StByte;
            if (t == typeof(string))
                return ItemType.StNVarChar;
            if (t == typeof(byte[]))
                return ItemType.StVarByte;
            if (t == typeof(float))
                return ItemType.StFloat;
            if (t == typeof(double))
                return ItemType.StDouble;
            if (t == typeof(Guid))
                return ItemType.StUuid;
            if (t == typeof(ReferenceMeta))
                return ItemType.Reference;
            if (t == typeof(ForeignMeta))
                return ItemType.Foreign;
            throw new SudbException(ErrorCode.TypeResolving);
        }

    }
}