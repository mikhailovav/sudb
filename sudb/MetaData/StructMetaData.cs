using System.Collections.Generic;
using System.Linq;

namespace Sudb.MetaData
{
    /// <summary>
    /// ���������� � ���������� ���������
    /// </summary>
    public class StructMetaData
    {
        private readonly List<ProtoField> _fieldsOverride;
        //private List<ProtoField> _fields;

        /// <summary>
        /// ��� ��������� ���������
        /// </summary>
        public readonly string Name;

        /// <summary>
        /// ��� ���������, �����������. 
        /// </summary>
        public readonly string ProtoName;

        public readonly ushort Protokey;

        public readonly string Filepath;

        /// <summary>
        /// ������ ��������� ��� ������ �������� ��� ����� ������
        /// </summary>
        public readonly uint ProtoSubtype;


        public uint LastKey { get; private set; }

        public uint StoreCount { get; set; }

        //public StructureMeta(string name, string protoName, ushort protokey, uint protoSubtype, ushort version)
        //{
        //    Name = name;
        //    ProtoName = protoName;
        //    Protokey = protokey;
        //    ProtoSubtype = protoSubtype;
        //    Version = version;
        //    _fieldsOverride = new List<StructureProtoField>();
        //}
        //public StructMetaData(string name, string protoName, ushort protokey, uint protoSubtype, ProtoField[] overrides, uint storeCount)
        //    :this(name,protoName,protokey,protoSubtype,overrides)
        //{
        //    StoreCount = storeCount;
        //}
        public StructMetaData(string filepath,string name, string protoName, ushort protokey, uint protoSubtype, ProtoField[] overrides)
        {
            Name = name;
            ProtoName = protoName;
            Protokey = protokey;
            ProtoSubtype = protoSubtype;
            Filepath = filepath;
            _fieldsOverride = new List<ProtoField>(overrides);
            //_fields = overrides.ToList();
            //_fields.AddRange(protoFields.Where(p=>overrides.All(a=>a.Name!=p.Name)));
        }

        internal uint GetNewKey()
        {
            LastKey++;
            return LastKey;
        }


        public ProtoField[] FieldsOverride => _fieldsOverride.ToArray();
        //public ProtoField[] Fields => _fields.ToArray();
    }
}