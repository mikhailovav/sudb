﻿using System.Collections.Generic;

namespace Sudb.MetaData
{
    /// <summary>
    /// абстрактная структура (прототип) на базе которого создается структура
    /// </summary>
    public class ProtoMetaData
    {
        private List<ProtoField> _fields;
        public readonly string Name;
        public readonly ushort Key;

        public ProtoField[] Fields => _fields.ToArray();

        public ProtoMetaData(string name, ushort key, ProtoField[] fields)
        {
            Name = name;
            Key = key;
            _fields = new List<ProtoField>(fields);
            Structures = new Dictionary<uint, StructMetaData>();
        }
        public ProtoMetaData(string name, ushort key, ProtoField[] fields, StructMetaData[] structs)
        {
            Name = name;
            Key = key;
            _fields = new List<ProtoField>(fields);
            Structures = new Dictionary<uint, StructMetaData>();
            foreach (var meta in structs)
            {
                Structures.Add(meta.ProtoSubtype,meta);
            }
        }


        internal readonly Dictionary<uint, StructMetaData> Structures;
    }
}