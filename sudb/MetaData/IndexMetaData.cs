using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Linq;
using System.Text;

namespace Sudb.MetaData
{
    internal class LocationSelector
    {
        public readonly byte StoreNumber;
        public readonly ulong Position;

        public LocationSelector(byte storeNumber, ulong position)
        {
            StoreNumber = storeNumber;
            Position = position;
        }
    }
    internal class IndexMetaData
    {
        public readonly string Name;

        public readonly string ProtoName;
        public readonly ushort Protokey;


        public  Dictionary<uint, LocationSelector> Index { get; private set; }
        public readonly string Filepath;

        public readonly IndexType Type;

        public readonly uint Offset;

        public IndexMetaData(string filepath,string name, string protoName, ushort protokey, IndexType type, uint offset)
        {
            Name = name;
            ProtoName = protoName;
            Protokey = protokey;
            Type = type;
            Offset = offset;
            Filepath = filepath;
            Index = new Dictionary<uint, LocationSelector>();
        }

        public void Load(Stream stream)
        {
           
        }

        public ulong GetPosition(uint key)
        {
            var en = Index.Keys.ToArray();
            uint cur=0;
            for (var i = 0; i < en.Length; i++)
            {
                cur = en[i];
                if (en.Length == i+1)
                {
                    return Index[cur].Position + Convert.ToUInt64((key - cur) * Offset);
                }
                var next = en[i + 1];
                if (key >= cur && key < next)
                {
                   
                }
            }
            if (cur == 0)
            {
                throw new SudbException(ErrorCode.ClusteredIndexKeyNotFound);
            }
            return Index[cur].Position + (key - cur) * Offset;
        }

        public void Set(uint key, ulong pos, byte n)
        {
            Index[key]=new LocationSelector(n,pos);
        }
    }
}