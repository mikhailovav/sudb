﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudb.MetaData
{
    /// <summary>
    /// Тип поля внутри структуры
    /// </summary>
    public enum ItemType : byte
    {
        Null = 0x00,
        StShort = 0x01,
        StInt32 = 0x02,
        StInt64 = 0x03,

        StBool = 0x04,
        StByte = 0x05,

        StNVarChar = 0x11,
        StVarByte = 0x12,


        StFloat = 0x21,
        StDouble = 0x22,

        StUuid = 0x31,


        /// <summary>
        /// Вместо данных по значению может содержать ссылку на другой объект (один к одному) (многое к одному)
        /// </summary>
        Reference = 0x51,

        /// <summary>
        /// Вместо данных по значению и ссылки может содержать отношение (один к многим)
        /// </summary>
        Foreign = 0x52
    }

    public class ReferenceMeta
    {
        public string ProtoName { get; set; }
        public ushort Protokey { get; set; }
        public uint ObjectKey { get; set; }
    }

    public class ForeignMeta
    {
        public uint ForeignKey { get; set; }
    }

    internal class StructureProtoPre
    {
        public string ObjectType { get; set; }
        public ProtoFieldForType[] Fields { get; set; }

        public ushort Protokey { get; set; }
        public uint ProtoSubtype { get; set; }

        public StructureProtoPre(string type, ProtoFieldForType[] fields)
        {
            ObjectType = type;
            Fields = fields;
        }

        public bool CheckProto(ProtoField[] protoFields)
        {
            var result = new List<ProtoFieldForType>();
            var check = protoFields.ToDictionary(protoField => protoField.Name, protoField => false);
            foreach (var field in Fields)
            {
                var protoField = protoFields.FirstOrDefault(f => f.Name == field.Name && f.BaseType == field.BaseType);
                if (protoField == null)
                {
                    result.Add(field);
                }
                else check[protoField.Name] = true;
            }
            this.Fields = result.ToArray();
            return check.All(a => a.Value);
        }

        public bool CheckStructure(ProtoField[] fields)
        {
            //TODO:изменить метод проверки, он может легко ошибиться
            var check = fields.Select(s => s.Name).ToDictionary(d => d, d=>false);
            foreach (var field in this.Fields)
            {
                if (check.ContainsKey(field.FieldName))
                    check[field.FieldName] = true;
                else return false;                
            }
            return check.All(a => a.Value);
        }
    }
    internal class ProtoFieldForType
    {
        public string Name { get; set; }
        public string FieldName { get; set; }
        public ItemType BaseType { get; set; }
        //public ushort VarLength { get; set; }
        public Type PropertyType { get; set; }
        public readonly uint VarLength;

        public ProtoFieldForType(string name, string fieldName, ItemType baseType, Type propertyType, uint varLength)
        {
            Name = name;
            BaseType = baseType;
            //VarLength = varLength;
            PropertyType = propertyType;
            VarLength = varLength;
            FieldName = fieldName;
        }
        
    }

    public class ProtoField
    {
        public string Name { get; set; }
        public ItemType BaseType { get; set; }
        public ushort Length { get; set; }

        public ProtoField(string name, ItemType type)
        {
            Name = name;
            BaseType = type;
            var btype = (byte)type;
            if (btype > 10 && btype < 20)
                throw new SudbException(ErrorCode.TypeVarLenType);
            switch (BaseType)
            {
                case ItemType.Null:
                    Length = 0;
                    break;
                case ItemType.StShort:
                    Length = sizeof(short);
                    break;
                case ItemType.StInt32:
                    Length = sizeof(Int32);
                    break;
                case ItemType.StInt64:
                    Length = sizeof(Int64);
                    break;
                case ItemType.StBool:
                    Length = sizeof(bool);
                    break;
                case ItemType.StByte:
                    Length = sizeof(byte);
                    break;
                case ItemType.StFloat:
                    Length = sizeof(float);
                    break;
                case ItemType.StDouble:
                    Length = sizeof(double);
                    break;
                case ItemType.StUuid:
                    Length = 16;
                    break;
                case ItemType.Reference:
                    //protokey+protosubtype+rowkey
                    Length = sizeof(ushort) + sizeof(uint) + sizeof(uint);
                    break;
                case ItemType.Foreign:
                    Length = sizeof(uint);
                    break;
                default:
                    throw new SudbException(ErrorCode.TypeResolving);
            }
        }
        public ProtoField(string name, ItemType type, ushort length)
        {
            Name = name;
            BaseType = type;
            var btype = (byte)type;
            if (btype <= 10 && btype >= 20)
                throw new SudbException(ErrorCode.TypeVarLenType);
            Length = length;
        }
    }



    public enum IndexType : byte
    {
        FieldIndex,
        ReferenceIndex,
        ForeignIndex,
        ClusteredIndex,
        SystemClusteredIndex
    }

    internal class ObjectData
    {

        public ObjectData(ushort protoKey, uint protoSubtype, ProtoFieldForType[] fields, Type t, object obj, ModificationKey state, uint key)
        {
            //BUG: проверить чтение-запись данных, прототипные поля не пишутся
            this.Protokey = protoKey;
            this.ProtoSubtype = protoSubtype;
            this.Data=new ExpandoObject();
            var data = Data as IDictionary<string, object>;
            foreach (var field in fields)
            {
                var prop = t.GetProperty(field.FieldName);
                
                data[field.Name] = prop.GetValue(obj);
            }
            State = state;
            Key = key;
            throw new NotImplementedException("проверить чтение-запись данных, прототипные поля не пишутся");
        }

        public ObjectData(ushort protoKey, uint protoSubtype, ProtoField[] fields, IDictionary<string, object> obj, ModificationKey state, uint key)
        {
            this.Protokey = protoKey;
            this.ProtoSubtype = protoSubtype;
            this.Data=new ExpandoObject();
            var data = Data as IDictionary<string, object>;
            foreach (var field in fields)
            {
                data[field.Name] = obj[field.Name];
            }
            State = state;
            Key = key;
        }

        public readonly ushort Protokey;
        public readonly uint ProtoSubtype;

        internal readonly uint Key;

        public ExpandoObject Data { get; set; }

        public ModificationKey State { get; set; }
    }

    internal class StructSelector
    {
        public readonly ushort Protokey;
        public readonly uint ProtoSubtype;

        public StructSelector(ushort protokey, uint protoSubtype)
        {
            Protokey = protokey;
            ProtoSubtype = protoSubtype;
        }
    }
    internal class StoreSelector: StructSelector
    {
        public readonly byte Number;

        public StoreSelector(ushort protokey, uint protoSubtype, byte number):base(protokey,protoSubtype)
        {
            Number = number;
        }
    }


    public enum ModificationKey
    {
        None=0,
        Modified=1,
        Deleted=2,
        Added=3
    }
}
