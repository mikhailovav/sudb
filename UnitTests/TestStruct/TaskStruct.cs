﻿namespace UnitTests.TestStruct
{
    public interface ITaskStruct
    {
        int Id { get; set; }
        string Task { get; set; }
        bool Checked { get; set; }
    }

    public class TaskStruct : ITaskStruct
    {
        public TaskStruct()
        {
            
        }
        public TaskStruct(int id, string task, bool @checked)
        {
            Id = id;
            Task = task;
            Checked = @checked;
        }

        public int Id { get; set; }

        [Sudb.Formatter.SudbFieldLength(260)]
        public string Task { get; set; }

        public bool Checked { get; set; }
    }

    public class CqTaskStruct : ITaskStruct
    {
        public CqTaskStruct()
        {
            
        }
        public CqTaskStruct(int id, string task, bool @checked, double cq)
        {
            Id = id;
            Task = task;
            Checked = @checked;
            Cq = cq;
        }

        public int Id { get; set; }

        [Sudb.Formatter.SudbFieldLength(260)]
        public string Task { get; set; }

        public bool Checked { get; set; }

        public double Cq { get; set; }
    }
}