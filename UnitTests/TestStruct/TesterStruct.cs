﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.TestStruct
{
    public interface ITesterStruct
    {
        int Id { get; set; }
        string Name { get; set; }
        string Avatar { get; set; }
        double Skill { get; set; }
    }

    public class TesterStruct : ITesterStruct
    {
        public TesterStruct()
        {
            
        }
        public TesterStruct(int id, string name, string avatar, int age, double skill)
        {
            Id = id;
            Name = name;
            Avatar = avatar;
            Age = age;
            Skill = skill;
        }

        public int Id { get; set; }
        [Sudb.Formatter.SudbFieldLength(20)]
        public string Name { get; set; }

        [Sudb.Formatter.SudbFieldLength(60)]
        public string Avatar { get; set; }

        public int Age { get; set; }

        public double Skill { get; set; }


    }

    public class SeniorTesterStruct : ITesterStruct
    {
        public SeniorTesterStruct()
        {
            
        }
        public SeniorTesterStruct(int id, string name, double skill, Guid seniorGuid, string avatar)
        {
            Id = id;
            Name = name;
            Skill = skill;
            SeniorGuid = seniorGuid;
            Avatar = avatar;
        }

        public int Id { get; set; }

        [Sudb.Formatter.SudbFieldLength(20)]
        public string Name { get; set; }

        [Sudb.Formatter.SudbFieldLength(60)]
        public string Avatar { get; set; }

        public double Skill { get; set; }

        public Guid SeniorGuid { get; set; }
    }
}
