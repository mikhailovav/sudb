﻿using System;
using System.ComponentModel.Design.Serialization;
using System.IO;
using System.IO.Compression;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sudb.Formatter;
using Sudb.MetaData;
using UnitTests.TestStruct;

namespace UnitTests
{
    [TestClass]
    public class GeneralTests
    {

        public static string Root =>  Directory.GetCurrentDirectory() + "/" + "TestData";
        [TestMethod]
        public void CreateDbTest()
        {
            if (!Directory.Exists(Root))
            {
                Directory.CreateDirectory(Root);
            }
            const string file = "CreateDbTest";

            if (File.Exists(Path.Combine(Root + "/", file + ".sudb")))
            {
                File.Delete(Path.Combine(Root + "/", file + ".sudb"));
                foreach (var f in Directory.GetFiles(Path.Combine(Root + "/", file + "_data")))
                {
                    File.Delete(f);
                }
            }

            var db = DatabaseMetaData.Create(file, file + ".sudb", Root);
            
            db.FixChanges();
        }

        [TestMethod]
        public void GeneralTest()
        {
            if (!Directory.Exists(Root))
            {
                Directory.CreateDirectory(Root);
            }
            const string file = "GeneralTest";

            if (File.Exists(Path.Combine(Root + "/", file + ".sudb")))
            {
                File.Delete(Path.Combine(Root + "/", file + ".sudb"));
                foreach (var f in Directory.GetFiles(Path.Combine(Root + "/", file + "_data")))
                {
                    File.Delete(f);
                }
            }

            var db = DatabaseMetaData.Create(file, file + ".sudb", Root);
            db.AddProto("Task", new[]
            {
                new ProtoField("Id", ItemType.StInt32),
                new ProtoField("Task", ItemType.StNVarChar,260),
                new ProtoField("Checked", ItemType.StBool)
            });
            db.AddProto("Tester", new[]
            {
                new ProtoField("Id", ItemType.StInt32),
                new ProtoField("Name", ItemType.StNVarChar,20),
                new ProtoField("Avatar", ItemType.StNVarChar,60),
                new ProtoField("Skill", ItemType.StDouble)
            });

            db.AddStructure("Task", new ProtoField[0]);
            db.AddStructure("Tester", new[]
            {
                new ProtoField("SeniorGuid", ItemType.StUuid),
            });
            db.AddStructure("Task", new[]
            {
                new ProtoField("SeniorGuid", ItemType.StUuid),
            });
            db.AddStructure("Tester", new[]
            {
                new ProtoField("Age", ItemType.StDouble),
            });

            db.InsertObject("Tester", new TesterStruct(1,"tester1","ava1",20,33f));
            db.InsertObject("Tester", new TesterStruct(2,"tester2","ava2",25,43f));

            db.InsertObject("Tester", new SeniorTesterStruct(2,"tester3",84f,Guid.NewGuid(),"ava3"));
            db.InsertObject("Tester", new SeniorTesterStruct(2, "tester4", 87f,Guid.NewGuid(),"ava4"));


            db.InsertObject("Task", new TaskStruct(2, "Complete tis task", false));
            db.InsertObject("Task", new TaskStruct(2, "Pass all tasks", false));

            db.InsertObject("Task", new CqTaskStruct(2, "Compile this project", false,16f));
            db.InsertObject("Task", new CqTaskStruct(2, "Create another test", false,18f));

            db.FixChanges();
        }
    }
}
